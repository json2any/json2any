import json
from argparse import ArgumentParser, Namespace
from pathlib import Path
from typing import List, Optional

from json2any_plugin.AbstractArgParser import AbstractArgParser
from json2any_plugin.AbstractDataProvider import AbstractDataProvider, FORBIDDEN_KEYS
from json2any_plugin.AbstractHelperProvider import AbstractHelperProvider
from json2any_plugin.AbstractTemplateProvider import AbstractTemplateProvider
from marshmallow_dataclass import class_schema
from pkg_resources import iter_entry_points

from json2any.DataLoaderException import DataLoaderException
from json2any.JinjaExecutor import JinjaExecutor
from json2any.RunsDescriptor import RunsDescriptor
from json2any.data_provider.JSONDataProvider import JSONDataProvider

from json2any.helper_provider.ExampleHelperProvider import ExampleHelperProvider
from json2any.template_provider.FileSystemTemplateProvider import FileSystemTemplateProvider


def load_runs_descriptor(rds_file: Path) -> RunsDescriptor:
    if not rds_file.is_file():
        raise ValueError
    with rds_file.open(mode='r') as f:
        j_data = json.load(f)
        schema = class_schema(RunsDescriptor)()
        runs_descriptor = schema.load(j_data)
        return runs_descriptor


class Json2Any(AbstractArgParser):

    def __init__(self):

        self.helper_providers: List[AbstractHelperProvider] = [ExampleHelperProvider()]
        self.data_providers: List[AbstractDataProvider] = [JSONDataProvider()]
        self.template_providers: List[AbstractTemplateProvider] = [FileSystemTemplateProvider()]

        for entry_point in iter_entry_points(group='json2any.plugin', name=None):
            el_class = entry_point.load()
            if issubclass(el_class, AbstractHelperProvider):
                self.helper_providers.append(el_class())
            elif issubclass(el_class, AbstractDataProvider):
                self.data_providers.append(el_class())
            elif issubclass(el_class, AbstractTemplateProvider):
                self.template_providers.append(el_class())
            pass

        self.active_data_providers: List[AbstractDataProvider] = []
        self.active_template_providers: List[AbstractTemplateProvider] = []

        self.rds: Optional[RunsDescriptor] = None
        self.output_dir: Optional[Path] = None
        pass

    def init(self):
        for loader in self.data_providers:
            try:
                loader.init()
            except Exception as ex:
                raise DataLoaderException('Failed to initialise data loader: "%s"' % loader.__class__.__name__) from ex

        for loader in self.template_providers:
            try:
                loader.init()
            except Exception as ex:
                raise DataLoaderException(
                    'Failed to initialise template loader: "%s"' % loader.__class__.__name__) from ex

    def update_arg_parser(self, parser: ArgumentParser) -> None:
        parser.add_argument('out_dir', help='Path to output directory')
        parser.add_argument('-r', '--rds-file', help='Path to runs descriptor file', required=True)

        for loader in self.data_providers:
            loader.update_arg_parser(parser)

        for loader in self.template_providers:
            loader.update_arg_parser(parser)

    def process_args(self, args: Namespace) -> bool:
        self.process_args_data_loaders(args)
        self.process_args_template_loaders(args)
        self.rds = load_runs_descriptor(Path(args.rds_file))

        self.output_dir = Path(args.out_dir)

        if not self.output_dir.is_dir():
            self.output_dir.mkdir(parents=True)

        if len(self.active_data_providers) == 0:
            raise Exception('Please specify at least one method of loading data. (no active data loader)')

        if len(self.active_template_providers) != 1:
            raise Exception('Please specify only one method of template loading option. (no active template loader)')

        return True

    def process_args_data_loaders(self, args):
        for loader in self.data_providers:
            is_active = loader.process_args(args)
            if is_active:
                self.active_data_providers.append(loader)

    def process_args_template_loaders(self, args):
        for loader in self.template_providers:
            is_active = loader.process_args(args)
            if is_active:
                self.active_template_providers.append(loader)

    def run(self):
        data = self.load_data()

        loader = self.active_template_providers[0].get_loader()
        executor = JinjaExecutor(loader, self.rds, data, out_dir=self.output_dir)
        for hp in self.helper_providers:
            helpers = hp.get_helpers()
            executor.environment.globals.update(helpers)
        executor.render()

    def load_data(self):
        loaded_datas = [ldr.load_data() for ldr in self.active_data_providers]
        data = {}
        for data_dict in loaded_datas:
            for key, data_item in data_dict.items():
                if key in data:
                    raise Exception('Duplicate data key "%s"' % key)
                if key in FORBIDDEN_KEYS:
                    raise Exception('Forbidden data key "%s"' % key)
                data[key] = data_item
        return data
