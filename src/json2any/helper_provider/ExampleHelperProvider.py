from argparse import Namespace, ArgumentParser
from typing import Dict, Callable

from json2any_plugin.AbstractHelperProvider import AbstractHelperProvider


def to_upper(s: str):
    return s.upper()


class ExampleHelperProvider(AbstractHelperProvider):
    def init(self) -> None:
        pass

    def update_arg_parser(self, parser: ArgumentParser) -> None:
        pass

    def process_args(self, args: Namespace) -> bool:
        return True

    def get_helpers(self) -> Dict[str, Callable]:
        helpers = {"to_upper": to_upper}
        return helpers
