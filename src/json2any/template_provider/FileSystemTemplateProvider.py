from abc import ABC
from argparse import Namespace, ArgumentParser
from pathlib import Path
from typing import List

from jinja2 import BaseLoader, FileSystemLoader
from json2any_plugin.AbstractTemplateProvider import AbstractTemplateProvider


class FileSystemTemplateProvider(AbstractTemplateProvider):
    def __init__(self):
        self.template_paths: List[Path] = []

    def init(self):
        pass

    def update_arg_parser(self, parser: ArgumentParser) -> None:
        parser.add_argument('-t', '--template-dir', help='Path to template directory', type=Path,
                            action='append')

    def process_args(self, args: Namespace) -> bool:
        if args.template_dir is None:
            return False

        for template_path in args.template_dir:
            template_path: Path
            if not template_path.is_dir():
                raise NotADirectoryError(f'{template_path} is not a directory')
            self.template_paths.append(template_path)
        return True

    def get_loader(self) -> BaseLoader:
        return FileSystemLoader(self.template_paths)
