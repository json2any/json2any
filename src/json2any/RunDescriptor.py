from dataclasses import dataclass
from pathlib import Path
from typing import List, Any, Optional


@dataclass
class RunDescriptor:
    name: str
    template: str
    description: Optional[str] = None
    query: Optional[str] = None
    query_for_each: bool = False
    output_file_pattern: Optional[str] = None
    output_override: bool = True
    run_data: Any = None
    enabled: bool = True
