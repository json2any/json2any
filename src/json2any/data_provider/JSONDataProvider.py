import json
from argparse import ArgumentParser, Namespace
from pathlib import Path
from typing import Dict, Any

from json2any_plugin.AbstractDataProvider import AbstractDataProvider


class JSONDataProvider(AbstractDataProvider):

    def __init__(self):
        self.input_files: Dict[str, Path] = {}

    def init(self):
        pass

    def update_arg_parser(self, parser: ArgumentParser):
        parser.add_argument('-j', '--input-json', action='append',
                            help='Data Loader: input JSON file with data key: "--input-json key:/path/to/file.json"')
        pass

    def process_args(self, args: Namespace) -> bool:
        if args.input_json is None:
            return False

        for item in args.input_json:
            item: str
            col_idx = item.find(':')
            if col_idx == -1:
                raise ValueError(f'Invalid "--input-json" value : {item}')
            key = item[0:col_idx]
            path = Path(item[col_idx + 1:])
            if not path.exists():
                raise ValueError(f'Input file "{path}" does not exist')
            if key in self.input_files:
                raise ValueError(f'Duplicate key "{key}" in "--input-json"')
            self.input_files[key] = Path(path)

        return True

    def load_data(self) -> Dict[str, Any]:
        data: Dict[str, Any] = {}
        for key, path in self.input_files.items():
            with open(path, 'r') as f:
                item_data = json.load(f)
                data[key] = item_data

        return data
