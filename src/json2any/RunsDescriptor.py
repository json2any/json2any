from dataclasses import dataclass, field
from typing import List, Optional

from json2any.RunDescriptor import RunDescriptor


@dataclass
class RunsDescriptor:
    name: str
    description: Optional[str]
    runs: List[RunDescriptor] = field(default_factory=list)
