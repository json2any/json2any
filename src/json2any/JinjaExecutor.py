import jsonpath_ng as jpng
from logging import getLogger
from pathlib import Path
from typing import Any, Dict, Optional

from jinja2 import Environment, BaseLoader
from json2any_plugin.AbstractDataProvider import DATA_KEY_RUNS, DATA_KEY_RUN, DATA_KEY_QUERY

from json2any.RunDescriptor import RunDescriptor
from json2any.RunsDescriptor import RunsDescriptor

RUN_DESCRIPTOR_FILE_DEFAULT = 'run_descriptor.json'

logger = getLogger('JinjaExecutor')


class JinjaExecutor:
    def __init__(self, template_loader: BaseLoader, runs_descriptor: RunsDescriptor, data: Dict[str, Any],
                 out_dir: Optional[Path] = None):

        self.environment = Environment(loader=template_loader)
        self.runs_descriptor: RunsDescriptor = runs_descriptor
        self.data = data
        self.out_dir = (out_dir or Path('.')).absolute()

        self.validate_runs()

    def render(self):
        for run in self.runs_descriptor.runs:
            if not run.enabled:
                continue

            out_dir = self.out_dir.absolute()
            if not out_dir.exists():
                out_dir.mkdir(parents=True)

            query_data = self.data_query(run)
            if run.query_for_each:
                if not isinstance(query_data, (list, tuple)):
                    raise ValueError(
                        f'Invalid query "{run.query}"; with foreach=True, the query must return a list object')
                for qd in query_data:
                    self.render_run_with_query_data(out_dir, qd, run)
            else:
                self.render_run_with_query_data(out_dir, query_data, run)

        pass

    def render_run_with_query_data(self, out_dir: Path, qd: Dict[str, Any], run: RunDescriptor):
        file_name_template = self.environment.from_string(run.output_file_pattern)
        renderer_args = self.data
        renderer_args[DATA_KEY_RUNS] = self.runs_descriptor
        renderer_args[DATA_KEY_RUN] = run
        renderer_args[DATA_KEY_QUERY] = qd
        file_name = file_name_template.render(renderer_args)
        out_file = out_dir / file_name

        if out_file.exists() and not run.output_override:
            return

        out_file_dir = out_file.parent
        if not out_file_dir.exists():
            out_file_dir.mkdir(parents=True)
        template = self.environment.get_template(run.template)
        content = template.render(renderer_args)
        with out_file.open('w') as f:
            f.write(content)

    def data_query(self, run: RunDescriptor):
        if run.query is None:
            return None

        matcher = jpng.parse(run.query)
        values = [m.value for m in matcher.find(self.data)]
        if len(values) == 0:
            logger.warning('Data query "%s" returned empty list', run.query)

        return values[0]

    def validate_runs(self):

        if len(self.runs_descriptor.runs) == 0:
            raise ValueError('No runs found in runs descriptor: %s', self.runs_descriptor)

        pass
