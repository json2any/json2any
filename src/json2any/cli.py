from argparse import ArgumentParser
from logging import INFO, DEBUG, StreamHandler, basicConfig, getLogger

from marshmallow_dataclass import class_schema

from json2any.Json2Any import Json2Any

logger = getLogger('json2any')


def setup_logging():
    basicConfig(
        level=INFO,
        format="%(asctime)s [%(levelname)s] %(message)s",
        handlers=[
            StreamHandler()
        ]
    )


def main():
    setup_logging()

    parser = ArgumentParser(description='Convert JSON to any format', prog='json2any')
    parser.add_argument('-v', '--verbose', help='Verbose output', action='store_true')
    parser.add_argument('-d', '--dump-schema', help='Dump Execution schema', action='store_true')

    j2a = Json2Any()
    j2a.init()
    j2a.update_arg_parser(parser)

    args = parser.parse_args()
    if args.verbose:
        logger.setLevel(DEBUG)
    else:
        logger.setLevel(INFO)

    if args.dump_schema:
        from json2any.RunsDescriptor import RunsDescriptor
        schema = class_schema(RunsDescriptor)()
        return

    j2a.process_args(args)
    j2a.run()

    pass


if __name__ == '__main__':
    main()
