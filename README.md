# Introduction

`json2Any` is highly customisable command line utility for text/code generation.

# Features

* Multi template execution
* Jinja2 template engine behind scene
* data query capability
* Multiple data sources
* Flexible data source mapping
* Easily bring in your own data format
* Easily bring in your own template loader
* Easily bring in your own helper function